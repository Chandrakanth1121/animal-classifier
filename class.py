import tensorflow
import tensorflow.keras
import numpy as np
import pandas as pd
from tensorflow.keras import backend as k
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Activation,Dropout,Flatten,Dense
from tensorflow.python.keras.layers import Conv2D,MaxPooling2D
from tensorflow.python.keras.layers.core import Dense,Flatten
from keras.optimizers import adam
from tensorflow.python.keras.metrics import categorical_crossentropy
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.preprocessing import image
#from tensorflow.keras import *
from tensorflow.python.keras.layers.normalization import BatchNormalization
from tensorflow.python.keras.layers.convolutional import *
from matplotlib import pyplot as plt
from tensorflow.python.keras import backend as K
from sklearn.metrics import confusion_matrix
import itertools
import matplotlib.pyplot as plt

img_width,img_height=150,150
train_path='/Users/chcha/PycharmProjects/untitled/train'
validation_path='/Users/chcha/PycharmProjects/untitled/test'
nb_train_samples=1000
nb_validation_samples=100
epochs=50
batch_size=10

if K.image_data_format()=='channels_first':
    input_shape=(3,img_width,img_height)
else:
    input_shape=(img_width,img_height,3)

train_datagen=ImageDataGenerator(rescale=1./255,shear_range=0.2,zoom_range=0.2,horizontal_flip=True)
#shear_range=0.2,zoom_range=0.2,horizontal_flip=True
test_datagen=ImageDataGenerator(rescale=1./255)
train_generator=train_datagen.flow_from_directory(train_path,target_size=(img_width,img_height),classes=['cane','cavallo','elefante','farfalla','gallina','gatto','mucca','pecora','ragno','scoiattolo'],batch_size=batch_size,class_mode='binary')
validation_generator=test_datagen.flow_from_directory(train_path,target_size=(img_width,img_height),classes=['cane','cavallo','elefante','farfalla','gallina','gatto','mucca','pecora','ragno','scoiattolo'],batch_size=batch_size,class_mode='binary')
#,classes=['cane','cavallo','elefante','farfalla','gallina','gatto','mucca','pecora','ragno','scoiattolo']

model=Sequential()
model.add(Conv2D(32,(3,3),input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.summary()

model.add(Conv2D(32,(3,3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(64,(3,3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
#model.add(Dropout(0.25))


model.add(Conv2D(64, (3, 3)))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(128))
model.add(Activation("relu"))

model.add(Dense(128))
model.add(Activation("relu"))

model.add(Dense(13))
model.add(Activation("softmax"))

#model.add(Flatten())
#model.add(Dense(64))
#model.add(Activation('relu'))
#model.add(Dropout(0.5))
#model.add(Dense(1))
#model.add(Activation('sigmoid'))

model.summary()

model.compile(loss='sparse_categorical_crossentropy',optimizer='SGD',metrics=['accuracy'])

model.fit_generator(
    train_generator,
    steps_per_epoch=1000,
    epochs=50,
    validation_data=validation_generator,
    validation_steps=100
)

model.save_weights('first_try.h5')

img_pred=image.load_img('/Users/chcha/PycharmProjects/untitled/test/cane/OIF-e2bexWrojgtQnAPPcUfOWQ.jpeg',target_size=(150,150))
img_pred=image.img_to_array(img_pred)
img_pred=np.expand_dims(img_pred,axis=0)

rslt=model.predict(img_pred)
arr=np.rint(rslt)
print(arr)
print(rslt)